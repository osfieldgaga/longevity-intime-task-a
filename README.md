# Longevity InTime Test Task A

This is the repository for the flutter app of task A.

The prototype of the app can be found here:
https://www.figma.com/proto/jANZukzpJV6NOxfmlHZMnj/Longevity-InTime-Test-Task?page-id=13%3A348&node-id=13%3A351&viewport=390%2C359%2C0.43&scaling=min-zoom&starting-point-node-id=13%3A351

The APK file can be found here:
https://drive.google.com/file/d/17kEUG-_Qhi05P0jfA455Phuwm2S5kJfg/view?usp=sharing
