import 'package:flutter/material.dart';
import 'homepage.dart';
import 'design_system_const.dart';


class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({super.key});

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: paddingNormal),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Welcome to this demo',
                style: bodyBold.copyWith(color: grayDark),
              ),
              const SizedBox(
                height: spacingNormal,
              ),
              Text(
                'You can go access the homepage to a dummy medical app',
                style: bodyRegular.copyWith(
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: spacingGap,
              ),
              ElevatedButton(
                onPressed: () {
                  
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const HomepageScreen(),
                      ),
                    );
                  
                },
                style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      //side: BorderSide(color: Colors.red),
                    ),
                  ),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 40),
                  child: Text(
                          'Go to homepage',
                          style: bodyBold.copyWith(color: Colors.white),
                        ),
                ),
              ),
              const SizedBox(
                height: spacingNormal,
              ),
              
            ],
          ),
        ),
      ),
    );
    ;
  }
}
