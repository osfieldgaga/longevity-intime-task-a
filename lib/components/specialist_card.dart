import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:longevity_intime_task_a/design_system_const.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SpecialistCard extends StatelessWidget {
  const SpecialistCard({super.key, required this.specialist});

  final specialist;

  @override
  Widget build(BuildContext context) {
    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24.0),
                          color: Colors.white),
                      width: 282,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: <Widget>[
                            Row(children: <Widget>[
                              Container(
                                clipBehavior: Clip.hardEdge,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16.0),
                                    color: Colors.white),
                                child: Image.asset(specialist == 1? 'assets/doctor1.png' : 'assets/doctor2.png'),
                              ),
                              const SizedBox(
                                width: spacingMedium,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    specialist == 1? 'Royce J. Garza' : 'Misty L. Butler',
                                    style: bodyRegular,
                                  ),
                                  const SizedBox(
                                    height: spacingSmall,
                                  ),
                                  Text(
                                    specialist == 1? 'Specialist in heart' : 'Specialist in lungs',
                                    style: smallLight,
                                  )
                                ],
                              )
                            ]),
                            Divider(),
                            GestureDetector(
                              child: Expanded(
                                child: Text(
                                  'Book an appointment',
                                  style: smallLight.copyWith(color: primary),
                                ),
                              ),
                              onTap: () {
                                print('Booking appointment');
                                Fluttertoast.showToast(
                                    msg: "Whoops! Out of the scope of this demo",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: gray,
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                              },
                            )
                          ],
                        ),
                      ),
                    );
  }
}