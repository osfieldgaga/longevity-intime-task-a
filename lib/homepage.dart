import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:longevity_intime_task_a/components/specialist_card.dart';
import 'package:longevity_intime_task_a/design_system_const.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';


class HomepageScreen extends StatefulWidget {
  const HomepageScreen({super.key});

  @override
  State<HomepageScreen> createState() => _HomepageScreenState();
}

class _HomepageScreenState extends State<HomepageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryLightBlue,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(paddingNormal, 90, paddingNormal, 40),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Hello there',
                          style: heading1.copyWith(
                            color: Colors.black,
                          ),
                        ),
                        Text(
                          'Check out this amazing homepage!',
                          style: smallLight.copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                    Stack(alignment: Alignment.center, children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: primary,
                            borderRadius: BorderRadius.circular(50)),
                        height: 60,
                        width: 60,
                      ),
                      CircleAvatar(
                        backgroundImage: AssetImage('assets/profile_pic.png'),
                        radius: 24,
                      )
                    ])
                  ],
                ),
                const SizedBox(
                  height: spacingXLarge,
                ),
                Text(
                  'Vitals',
                  style: bodyBold,
                ),
                const SizedBox(
                  height: spacingXMedium,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 160,
                      height: 140,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xFF3683FC).withOpacity(0.3),
                              offset: const Offset(
                                0.0,
                                8.0,
                              ),
                              blurRadius: 20.0,
                              spreadRadius: 2.0,
                            )
                          ],
                          color: primary,
                          borderRadius: BorderRadius.circular(24.0)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 32, 16, 32),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    SvgPicture.asset(
                                      'assets/icons/heart.svg',
                                      semanticsLabel: 'heart',
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                'AVG',
                                style: smallLight.copyWith(color: Colors.white),
                              ),
                              const SizedBox(
                                height: spacingSmall,
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    '71',
                                    style: bodyBold.copyWith(color: Colors.white),
                                  ),
                                  const SizedBox(
                                    width: spacingNormal,
                                  ),
                                  Text(
                                    'BPM',
                                    style:
                                        smallLight.copyWith(color: Colors.white),
                                  )
                                ],
                              )
                            ]),
                      ),
                    ),
                    Container(
                      width: 160,
                      height: 140,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(24.0)),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 32, 16, 32),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      'SpO2',
                                      style: bodyBold.copyWith(color: grayDark),
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                'AVG',
                                style: smallLight.copyWith(color: grayDark),
                              ),
                              const SizedBox(
                                height: spacingSmall,
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    '98',
                                    style: bodyBold.copyWith(color: grayDark),
                                  ),
                                  const SizedBox(
                                    width: spacingNormal,
                                  ),
                                  Text(
                                    '%',
                                    style: smallLight.copyWith(color: grayDark),
                                  )
                                ],
                              )
                            ]),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: spacingXLarge,
                ),
                Text(
                  'Specialists',
                  style: bodyBold,
                ),
                const SizedBox(
                  height: spacingXMedium,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      SpecialistCard(specialist: 1),
                      const SizedBox(width: spacingMedium,),
                      SpecialistCard(specialist: 2),
                    ],
                  ),
                ),
                const SizedBox(
                  height: spacingXLarge,
                ),
                Text(
                  'Prescriptions',
                  style: bodyBold,
                ),
                const SizedBox(
                  height: spacingNormal,
                ),
                Container(
                  //height: 140,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24.0),
                        color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "From last appointment",
                                style: smallLight,
                              ),
                              SizedBox(height: spacingSmall,),
                              Text(
                                "Paracetamol 500mg\nMetagyl 400mg\nAerus 98mg\nSomething else",
                                style: bodyRegular,
                              ),
                            ],
                          ),
                          Divider(),
                          GestureDetector(
                            child: Expanded(
                              child: Text(
                                'Order prescription online',
                                style: smallLight.copyWith(color: primary),
                              ),
                            ),
                            onTap: () {
                              print('Ordering prescription online');
                              Fluttertoast.showToast(
                                  msg: "Whoops! Out of the scope of this demo",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: gray,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: spacingXLarge,
                ),
                Text(
                  'Log symptoms',
                  style: bodyBold,
                ),
                const SizedBox(
                  height: spacingNormal,
                ),
                Container(
                  height: 64,
                  child: GestureDetector(
                    onTap: () {
                              print('Log symptoms');
                              Fluttertoast.showToast(
                                  msg: "Whoops! Out of the scope of this demo",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: gray,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24.0),
                          color: Colors.white),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Select symptoms from the list',
                                style: bodyRegular,
                              ),
                              Icon(Icons.arrow_forward)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                
                const SizedBox(
                  height: spacingXLarge,
                ),
                Text(
                  'Recent activities',
                  style: bodyBold,
                ),
                const SizedBox(
                  height: spacingNormal,
                ),
                Container(
                  //height: 140,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24.0),
                        color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Fri. 21 Oct. 2022",
                                style: smallLight,
                              ),
                              SizedBox(height: spacingSmall,),
                              Text(
                                "Appointment with Dr. Garza",
                                style: bodyRegular,
                              ),
                            ],
                          ),
                          const SizedBox(height: spacingNormal,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Tue. 18 Oct. 2022",
                                style: smallLight,
                              ),
                              SizedBox(height: spacingSmall,),
                              Text(
                                "Regular check up",
                                style: bodyRegular,
                              ),
                            ],
                          ),
                          Divider(),
                          GestureDetector(
                            child: Expanded(
                              child: Text(
                                'See older events...',
                                style: smallLight.copyWith(color: primary),
                              ),
                            ),
                            onTap: () {
                              print('Seeing older events...');
                              Fluttertoast.showToast(
                                  msg: "Whoops! Out of the scope of this demo",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: gray,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
